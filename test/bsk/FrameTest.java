package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testFrameFirstThrow(){
		Frame frame= new Frame(2,4);
		
		assertEquals(2,frame.getFirstThrow());
	}
	
	
	@Test
	public void testFrameSecondThrow(){
		Frame frame= new Frame(2,4);
		
		assertEquals(4,frame.getSecondThrow());
	}
	
	@Test 
	public void testFrameGetScore(){
		Frame frame= new Frame(2,6);
		assertEquals(8,frame.getScore());
	}
	
	
}
