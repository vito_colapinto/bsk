package tdd.training.bsk;

public class Game {
	
	private Frame[] bGame=new Frame[10];
	private int counterFrames=0;
	private int score=0;
	private int fBThrow;
	private int sBThrow;
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		for(int i=0;i<10;i++) {
			bGame[i]=new Frame(0,0);
		}
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) {
			
		bGame[counterFrames]=frame;
		counterFrames++;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) {
		
		
		return bGame[index-1];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		fBThrow=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		sBThrow=secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		
		return fBThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		
		return sBThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() {
		
		boolean perfectScore=true;
		
		for(int j=0;j<10;j++) {
			
			if (!bGame[j].isStrike() && j<10) perfectScore=false;
		}
		if((getSecondBonusThrow()!=10) && (getFirstBonusThrow()!=10)) perfectScore=false;
		
		if(!perfectScore) {
		for(int i=0;i<10;i++) {
			
			calculateSpare(i);
			
			calculateStrike(i);
			
			score+=bGame[i].getScore();
			}
		
		return score;	
						}else {
								score=300;
								return score;
								}
		
	}

	
	
	private void calculateStrike(int i) {
		int bonus;
		
		if(bGame[i].isStrike()) {
			
			if(i==9) {
				bonus=getFirstBonusThrow()+getSecondBonusThrow();
				bGame[i].setBonus(bonus);
				
				}else {
					
					bonus=bGame[i+1].getFirstThrow()+bGame[i+1].getSecondThrow();
					bGame[i].setBonus(bonus);
			
			
			
					if(bGame[i+1].isStrike()) {
						bonus=10+bGame[i+2].getFirstThrow();
						bGame[i].setBonus(bonus);
				}
			}
		}
		
	}

	private void calculateSpare(int i) {
		int bonus;
		
		if(bGame[i].isSpare()) {
			
			if(i==9) {
				bonus=getFirstBonusThrow();
				bGame[i].setBonus(bonus);
			}else {
			
			bonus=bGame[i+1].getFirstThrow();
			bGame[i].setBonus(bonus);
			}
		}
		
	}

}
